# Installation #

Put this file somewhere in your load-path and put in your configuration

	(require 'solution-manager)

# Usage #

To run OmniSharpServer when .cs file is opening add to the csharp-mode-hooks

	(defun my-csharp-mode-fn ()
		(sm-start-server)
		(other)
		(functions))

	(add-hook 'csharp-mode-hook 'my-csharp-mode-fn t)

Also you have to specify the location of the OmniSharp.exe file:
	(setq sm-omnisharp-bin "/home/user/location/OmniSharp.exe")

or make sure that OmniSharp.exe is on your PATH.

# Obvious problems #

I bet there's a lot of projects that have sln files in single directory and that's not working right now.
My project at work just don't look like this.

I've changed job, no more Csharp