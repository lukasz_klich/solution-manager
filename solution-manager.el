;;; solution-manager,el --- omnisharp solution manager for omnisharp.el
;; Copyright (C) 2013 Łukasz Klich (GPLv3)
;; Author: Łukasz Klich
;; Version: 0.1
;; Url: https://github.com/kleewho/solution-manager
;; Package-Requires: ((omnisharp "1.4"))
;; Keywords: csharp c# IDE auto-complete intellisense
;; 
;;; Commentary:
;; This module in more final form should:
;; - run omnisharp server for curently opened C# file
;; - handle communication with multiple servers (I hate to have opened
;;   couple of instances of VS)
;;
;;; Code:

(require 'openwith)

(defvar sm-omnisharp-bin nil
  "Path to OmniSharpServer.  Use if it's not on PATH.")

(defvar sm-port 2000
  "Port which will be used for next OmniSharpServer.
This is the initial port on which omnisharp solution manager starts
opening OmniSharpServers.")

(defvar sm-temp-file "~/solution-manager-temp-runner.bat"
  "Windows only.  Path to file where solution manager can write.
On windows OmniSharpServer have to start by executable file.")

(defvar sm--servers (make-hash-table :test 'equal)
  "Lookup list to bind buffers to opened OmniSharpServers.")

(defun sm--openwith (bin port solution)
  "Run function depending on operating system to start OmniSharpServer.
Arguments: BIN is path to OmniSharp.exe,
PORT is port on which should OmniSharpServer listen,
SOLUTION is solution for OmniSharpServer."
  (let ((args (list bin "-p" (number-to-string port) "-s" solution)))
    (if (eq system-type 'windows-nt) (sm--run-windows args)
      (sm--run-linux args))))

(defun sm--run-linux (args)
  "Run mono executable with arguments.  All is provided as list ARGS."
  (openwith-open-unix "mono" args))

(defun sm--run-windows (args)
  "Create windows .bat file with ARGS write to it and run it as another process."
  (with-temp-file sm-temp-file
    (insert (apply 'concat (mapcar (lambda (arg) (concat arg " ")) args))))
  (openwith-open-windows sm-temp-file))

(defun sm-get-parent (dir)
  "Get parent directory for directory DIR."
  (file-name-directory (directory-file-name dir)))

(defun sm-find-solution (dir)
  "Search for solution moving up in directories DIR."
  (let ((solution (directory-files dir t "sln$")))
    (if solution (first solution)
      (sm-find-solution (sm-get-parent dir)))))
  
(defun sm-already-started-p (solution)
  "Check's if OmniSharpServer is already opened for SOLUTION."
  (if (gethash solution sm--servers) t
    nil))

(defun sm--start-server (bin port solution)
  "Run OmniSharpServer.
It run it like this: BIN -p PORT -s SOLUTION"
  (if (not (sm-already-started-p solution))
    (progn
      (sm--openwith bin port solution)
      (puthash solution port sm--servers)
      (sm--set-host-for-buffer port)
      (setq sm-port (1+ sm-port)))
    (sm--set-host-for-buffer (gethash solution sm--servers))))

(defun sm--get-host (port)
  "Provided with PORT return host adress."
  (concat "http://localhost:" (number-to-string port) "/"))

(defun sm--set-host-for-buffer (port)
  "Set local buffer var to proper PORT."
  (setq-local omnisharp-host (sm--get-host port)))

(defun sm--get-bin ()
  "Return OmniSharpServer path.
If sm-omnisharp-bin is defined and points on existing file it will use this
file to start OmniSharpServer, otherwise it will use the one on PATH"
  (if (and sm-omnisharp-bin (file-exists-p sm-omnisharp-bin)) sm-omnisharp-bin
      "OmniSharp.exe"))


(defun sm--get-buffer-dir ()
  "Get directory for csharp file."
  (file-name-directory (buffer-file-name (current-buffer))))

(defun sm-start-server ()
  "Function that should suite as a function for hook mode."
  (let ((solution (sm-find-solution (sm--get-buffer-dir))))
      (when solution
	(sm--start-server (sm--get-bin) sm-port solution))))

(provide 'solution-manager)
;;; solution-manager.el ends here
